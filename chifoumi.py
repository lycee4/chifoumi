from random import choice
ORDI = "ordinateur"
JOUEUR = "joueur"
NUL = "match nul"

def determiner_gagnant(choix_joueur, choix_ordi):
	'''
	fonction permettant de determiner qui gagne de l'ordinateur ou du joueur.
	'''
	if choix_joueur == "p":
		if choix_ordi == "p":
			gagnant = NUL
		elif choix_ordi == "f":
			gagnant = ORDI
		else:
			gagnant = JOUEUR
	elif choix_joueur == "f":
		if choix_ordi == "p":
			gagnant = JOUEUR
		elif choix_ordi == "f":
			gagnant = NUL
		else:
			gagnant = ORDI
	else:
			if choix_ordi == "p":
				gagnant = ORDI
			elif choix_ordi == "f":
				gagnant = JOUEUR 
			else:
				gagnant = NUL
	return gagnant
def jouer_un_tour():
	reponse = None
	while reponse is None:
		saisie = input("[p]ierre, [f]euille, [Ci]seaux :")
		saisie_en_majuscule = saisie.upper()
		if saisie_en_majuscule == "p" or saisie_en_majuscule == "PIERRE":
			reponse = "p"
		elif saisie_en_majuscule == "f" or saisie_en_majuscule == "FEUILLE":
			reponse = "f"
		elif saisie_en_majuscule == "c" or saisie_en_majuscule == "CISEAUX":
			reponse = "c"
		else:
			print("choix incorrect")
	choix_ordi = choice ([")Pierre", "Feuille", "Ciseaux"])
	print("je joue", choix_ordi)
	code_choix_ordi = choix_ordi [0]
	resultat = determiner_gagnant(reponse, code_choix_ordi)
	if resultat == JOUEUR:
		print("Vous gagnez")
	elif resultat == ORDI:
		print("je gagne")
	else:
		print("match nul")
	return resultat
def jouer_partie(nb_points):
	nb_points_joueur = 0
	nb_points_ordi = 0
	while nb_points_joueur < nb_points and nb_points_ordi < nb_points:
		gagnant = jouer_un_tour()
		if gagnant == ORDI:
			nb_points_ordi = nb_points_ordi + 1
		elif gagnant == JOUEUR:
			nb_points_joueur = nb_points_joueur + 1
	if nb_points_joueur > nb_points_ordi:
		print("vous gagnez", nb_points_joueur, "a", nb_points_ordi)
	else:
		print("je gagne", nb_points_ordi, "a", nb_points_joueur)
def jouer():
	nb_points_gagnants = int(input("En combien de coup voulez-vous ?"))
	jouer_partie(nb_points_gagnants)
if __name__ == '__main__':
	jouer()
